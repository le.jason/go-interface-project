package main

import (
	"fmt"
	"math"
)

type Circle struct {
	name   string
	radius float64
}

type Square struct {
	name string
	side float64
}

func (c Circle) getName() string {
	return c.name
}

func (s Square) getName() string {
	return s.name
}

func (c Circle) area() float64 {
	return math.Pi * c.radius * c.radius
}

func (s Square) area() float64 {
	return s.side * s.side
}

// Shape interface to represent all shapes
type Shape interface {
	getName() string
	area() float64
}

func findArea(s Shape) {
	fmt.Println("Area of "+s.getName()+" is: ", s.area())
}

func main() {
	square := Square{"Square", 5}
	circle := Circle{"Circle", 3.4}
	findArea(square)
	findArea(circle)
}
