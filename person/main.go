package main

import "fmt"

type Person struct {
	age  int
	name string
}

type SecretAgent struct {
	Person
	agentID  int
	codeName string
}

func (p Person) pSpeak() {
	fmt.Printf("Hello my name is: "+p.name+" and i am %d years old\n", p.age)
}

func (sa SecretAgent) saSpeak() {
	fmt.Printf("Hello this is: " + sa.codeName + " I cannot reveal my location however I am in danger")
}

func main() {
	person := Person{
		12,
		"Jason",
	}

	sa := SecretAgent{
		Person{
			18,
			"Jacob",
		},
		8877766,
		"Giraffe",
	}

	person.pSpeak()
	sa.saSpeak()
	fmt.Println(sa.name)
	sa.pSpeak()

}
